export function* generate(context, funcs, dialogData = {}) {
  for (let func of funcs) {
    dialogData = yield func({ ...context, ...dialogData });
  }
}

export const collector = (iterators, times) => new Promise((resolve, reject) =>
    resolve(
      [...Array(times)]
        .reduce(async (acc, _) => {
          const ctx = await acc;
          return await iterators.next({ ...ctx }).value;
        },
      {}
    )
  )
);
