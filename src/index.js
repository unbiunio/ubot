import {
  collector,
  generate,
} from './bot-util';

const _props = {
  setState: {
    value: function(state) {
      this.state = { ...this.state, ...state };
  }}
};

const _extends = (proto) => Object.create(proto, _props);

export const startConversation = _super => _extends({

  async answer(context) {
    const { did } = context;
    const ctx = { ...context, ...this.funcs(context) };
    if (did !== this.state.active.id) {
      await this.setActive(ctx, did);
    }

    await this.next({ ...context });
  },

  endDialog() {
    this.setState({ active: {} })
  },

  funcs(context) {
    return {
      send: this.send(context).bind(this),
      replace: this.replace(context).bind(this),
    }
  },

  next(context) {
    const _this = this;
    setTimeout(function() {
      _this.state.active.step.next(context)
    }, 0)
  },

  send(context) {
    return async function(m, data) {
      const message = typeof m === 'string' ? { text: m } : m;
      new Promise(resolve => context.sendMessage({ ...message, ...data }));
    }
  },

  state: {
    active: {}
  },

  async setActive(context, did) {
    const getDialog = _super.getDialogById.bind(_super);
    const foundDialog = getDialog(did);
    const steps = foundDialog.steps;
    const step = generate(
      {
        ...context,
        next: this.next.bind(this),
        endDialog: this.endDialog.bind(this),
      },
      steps
    );
    this.setState({ active: { ...foundDialog, step } });
  },

  replace(context) {
    return async function(did) {
      const ctx = { ...context, ...this.funcs(context) };
      this.setActive(ctx, did);
      this.next({ ...ctx });
    }
  },
});

export default (config) => _extends({

  getDialogById(did) {
    return this.state.dialogs.find(({ id }) => did === id)
  },

  start() { return startConversation(this) },

  register(dialog) {
    this.setState({ dialogs: [ ...this.state.dialogs, dialog ] })
  },

  registerAll(dialogs) {
    dialogs.forEach(this.register.bind(this))
  },

  remove(did) {
    const { dialogs } = this.state;
    this.setState({ dialogs: dialogs.filter(dialog => dialog.id !== did) });
  },

  sequentialLoad: (ctx, items) => collector(generate(ctx, items), items.length),

  state: {
    dialogs: []
  },

});
